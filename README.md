# Leaflet heatmap

A website written in django. Upload images to server, images is classified and all cracks is plottet on map as a heat-map point.

## Getting Started

Download repo and run ```pip install -r requirements.txt```, this will install nessecery packages. Should be run on a linux system, since ```celery 4.2.1``` does not work with ```django 2.1.7``` on windows(untested on mac os). Can be modified to work on windows by rolling back celery version and modify usage. To start webpage run ```python manage.py makemigrations map``` in project directory. Next run ```python manage.py migrate``` to migrate apps, then python manage.py runserver to start webpage. Then a celery worker needs to be started to allow offloading of processes, start worker with: ```celery -A map worker --loglevel=info```. To access webpage, open a web-browser and go to ```localhost:8000```.

### Prerequisites

A broker service to communicate with celery, using RabbitMQ in this version but others, like redis, can be used by changing ```app.conf.broker_url``` in map/celery.py.

## Authors

* **Eirik Gribbestad Gustafsson** - *Webpage* 
* **Sivert Løken** - *Image data extraction*
* **Ole-Martin Steinnes** - *Object-detection api*
