from django.shortcuts import render
from django.conf import settings

from .models import RoadImage
##from . import tasks

# Prints map and passes a coordinates list equal to number 
# of cracks from images
def print_map(request):
    roadImages = RoadImage.objects.all()
    len_roadImages = len(roadImages)
    coordinates = []
    
    unclassified_img = 0 
    used_img = 0
    
    for roadImage in roadImages:
        n = roadImage.get_n_dmg()
        coord = roadImage.get_coordinates()
              
        if 0 < n:
            used_img += 1
            for i in range(n):
                coordinates.append([coord['latitude'], coord['longditude']])
        if -1 == n:
            unclassified_img += 1	

       	roadImage.save()

    if settings.DEBUG:
        print('Used images: ', used_img)
        print('Classified images: ', (len(roadImages) - unclassified_img))
        print('Total images: ', len(roadImages))
        
    return render(request, 'map/map.html', {'coordinates':coordinates, 
                                            'used_img':used_img, 
                                            'unclassified_img':unclassified_img, 
                                            'total_img':len_roadImages})

def login(request):
    return render(request, 'login/login.html',{})

def home(request):
    roadImages = RoadImage.objects.all()
    coordinates = []

    for roadImage in roadImages:
        n = roadImage.get_n_dmg()
        coord = roadImage.get_coordinates()

        if 0 < n:
            for i in range(n):
                coordinates.append([coord['longditude'], coord['latitude']])
                
    return render(request, 'home/home.html', {'coordinates':coordinates})

def about(request):
    return render(request, 'about/about.html')
def guide(request):
    return render(request, 'guide/guide.html',{})

# Uploads files and spawns a proccess to classify image
def multiple_upload(request):
    if 'POST' == request.method:
        for img in request.FILES.getlist('file'):
            if img.name.lower().endswith('.jpg'):
                roadImage = RoadImage()
                roadImage.image = img
                roadImage.save()
                
                tasks.set_n_dmg.delay(roadImage.id)
            
            elif settings.DEBUG:
                print('Wrong file type, file not uploaded')



    return render(request, 'imgUpload/imgUpload.html', {})
