from django.contrib import admin
from . import models

# Registers model to admin page
admin.site.register(models.RoadImage)
