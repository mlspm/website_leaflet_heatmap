from __future__ import print_function
# This is a placeholder for a Google-internal import.
from grpc.beta import implementations
from grpc import RpcError
import glob
import numpy as np
import scipy
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2
import visualization_utilities as visutil
import label_map_util as lmu
from PIL import Image

def main_test():
    visu_test()

def visu_test():
    host = '23.102.61.248'
    port = '13000'
    input_image = '/home/osteinnes/testpascal'
    path_to_labels = '/home/osteinnes/label.pbtxt'

    channel = implementations.insecure_channel(host, int(port))
    stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

    # Create prediction request object
    request = predict_pb2.PredictRequest()

    # Specify model name (must be the same as when the TensorFlow serving serving was started)
    request.model_spec.name = 'gpu_serving_model'

    images = glob.glob(input_image + "/*.jpg")
    for image in images:
        img = scipy.misc.imread(image)
        request.inputs['inputs'].CopyFrom(tf.contrib.util.make_tensor_proto(img, shape=[1] + list(img.shape)))

        # Call the prediction server
        result = stub.Predict(request, 180.0)  # 10 secs timeout

        # Plot boxes on the input image
        category_index = lmu.create_category_index_from_labelmap(path_to_labels, False)
        boxes = result.outputs['detection_boxes'].float_val
        classes = result.outputs['detection_classes'].float_val
        scores = result.outputs['detection_scores'].float_val

        image_vis = visutil.visualize_boxes_and_labels_on_image_array(
            img,
            np.reshape(boxes,[100,4]),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=4)

        # Save inference to disk
        #scipy.misc.imsave('%s.jpg'%(image), image_vis)

        print(image_vis)

        bilde = Image.fromarray(image_vis, 'RGB')
        bilde.save('test.jpg')
        bilde.show()



if __name__ == '__main__':
    main_test()