from __future__ import print_function
# This is a placeholder for a Google-internal import.
import grpc as gr
from grpc import RpcError
from PIL import Image
import numpy as np
import tensorflow as tf
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc

# Comunicates with tensor serving api


class DamageDetector(object):
    stub = None
    host = None
    port = None

    # Setup, sets flag and create stub
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.stub = self.create_stub()

    # Create stub
    def create_stub(self):
        channel = gr.insecure_channel((str(self.host) + ':' + str(self.port)))
        return prediction_service_pb2_grpc.PredictionServiceStub(channel)

    # Returns number of damages detected
    def get_n_dmg(self, img):
        # Create prediction request object
        request = predict_pb2.PredictRequest()

        # Specify model name (must be the same as when the TensorFlow serving serving was started)
        request.model_spec.name = 'gpu_serving_model'

        # Initalize prediction
        # Specify signature name (should be the same as specified when exporting model)
        request.model_spec.signature_name = ""

        img = Image.open(img)
        shape = np.array(img).shape

        request.inputs['inputs'].CopyFrom(tf.make_tensor_proto(img, shape=[1] + list(shape)))

        img.close()

        try:  
            # Call the prediction server
            result = self.stub.Predict(request, 180.0)  # 10 secs timeout

            # Getts boxes
            boxes = result.outputs['detection_boxes'].float_val
            scores = result.outputs['detection_scores'].float_val

            pred_boxes = np.reshape(boxes, [100, 4])
            pred_scores = np.squeeze(scores)

            n_cracks = 0
            for i in range(pred_boxes.shape[0]):
                    # Discard any boxas under 50% certainty
                    if pred_scores[i] > 0.5:
                        n_cracks += 1

        except RpcError as ex:
            print('RpcError in object_detection_client')
            n_cracks = None

        # Returns number of predicted damages
        return n_cracks
