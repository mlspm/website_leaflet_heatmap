from django.apps import AppConfig

# Defines map app
class MapConfig(AppConfig):
    name = 'map'
