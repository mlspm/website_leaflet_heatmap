from django.urls import path, include
from . import views
from django.conf import settings
from django.conf.urls.static import static

# Url file to differentiate file from other url files 
app_name = 'base'

# Paths point to different url templates
urlpatterns = [
    path('', views.home, name='home'),
    path('map/', views.print_map, name='map'),
    path('upload/', views.multiple_upload, name='upload'),
    path('guide/', views.guide, name='guide'),
    path('about/', views.about, name='about'),
    path('login/', views.login, name='login')
]

# Paths to server media folder
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)