from django.db import models
from django.core.validators import int_list_validator
from .scripts import metadata_extractor

# Holds a road image and number of cracks in image
class RoadImage(models.Model):
    image = models.ImageField()
    nDmg = models.IntegerField(default=-1)

    # Returns coordinates from image, in key value pair.
    # Latitude      = coordinates['latitude']
    # Longditude    = coordinates['longditude']
    def get_coordinates(self):
        return metadata_extractor.get_dd(self.image)

    # Returns image
    def get_image(self):
        return self.image

    # Setts new image
    def set_image(self, img):
        self.image = img

    # Returns number of cracks in image
    def get_n_dmg(self):
        return self.nDmg

    # Set number of cracks in image
    def set_n_dmg(self, nDmg):
        self.nDmg = nDmg
        
    def __str__(self):
        return self.image.name