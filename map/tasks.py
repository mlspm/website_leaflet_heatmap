from celery import shared_task, Celery\

from .models import RoadImage
from .scripts.object_detection_client import DamageDetector

# Damage detector api communicater
detector = DamageDetector('23.102.61.248', '13000')

# Takes an image and communicates with server to classify the image
@shared_task
def set_n_dmg(imgId):
    roadImg = RoadImage.objects.get(pk=imgId)
    nCracks = detector.get_n_dmg(roadImg.get_image())
    if None != nCracks:
       roadImg.set_n_dmg(nCracks)
       roadImg.save()
    else: 
       print('Object detector returned None')


# Test class to test celery worker
@shared_task
def test_class(a, b):
    return a+b
